exports.server = {
	ip: '127.0.0.1',
	port: 8000,
	welcome: 'Welcome to Moss v1.0',
	database: 'mongodb://localhost/moss',
	debug: true
};