var socket;
var history = [];
var historyIndex = 0;
var historyCurrent;

function getSelectionText() {
	var text = "";
	if (window.getSelection) {
		text = window.getSelection().toString();
	} else if (document.selection && document.selection.type != "Control") {
		text = document.selection.createRange().text;
	}
	return text;
}

function writeCookie(name, value, days) {
	if (days) {
		var date = new Date();
		date.setTime(date.getTime() + (days * 24 * 60 * 60 * 1000));
		var expires = "; expires=" + date.toGMTString();
	}
	else {
		var expires = "";
	}
	document.cookie = name + "=" + value + expires + "; path=/";
}

function readCookie(name) {
	var nameEQ = name + "=";
	var ca = document.cookie.split(';');
	for (var i = 0; i < ca.length; i++) {
		var c = ca[i];
		while (c.charAt(0) == ' ') c = c.substring(1, c.length);
		if (c.indexOf(nameEQ) == 0) {
			return c.substring(nameEQ.length, c.length);
		}
	}
	return null;
}

function eraseCookie(name) {
	createCookie(name, "", -1);
}

var cmd;

$(function(){
	socket = io.connect('');
	cmd = $('#cmd');

	socket.on('terminal', function(response) {
		response = _.extend({styleClass: 'terminal'}, response);
		doScroll = $('#bottom').isOnScreen();
		var last = $('#terminal > div:last-child');
		if(response.styleClass == 'comms' && last.hasClass('comms')) {
			last.append('<br>' + response.output);
		}
		else {
			$('#terminal').append('<div class="' + response.styleClass + '">' + response.output + '</div>');
		}
		if(doScroll) {
			$('html, body').animate({scrollTop: $('#bottom').offset().top -20});
		}
		else {
			cmd.addClass('newmsg');
		}
		if(typeof response.state !== undefined) {
			cmd.data('state', response.state);
			switch(response.state) {
				case 'login':
					cmd.attr('type', 'text');
					cmd.attr('placeholder', 'Enter your username');
					break;
				case 'password':
					cmd.attr('type', 'password');
					cmd.attr('placeholder', 'Enter your password');
					break;
				default:
					cmd.attr('type', 'text');
					cmd.attr('placeholder', 'Command');
			}
		}
	});
	socket.on('disconnect', function () {
		cmd.addClass('disconnected');
		cmd.data('state', 'boot');
		cmd.attr('placeholder', 'Disconnected... please wait...');
	});
	doConnect = function() {
		cmd.removeClass('disconnected');
		var token = readCookie('logintoken'),
			user = readCookie('loginuser');
		cmd.data('state', 'boot');
		cmd.attr('placeholder', 'Attempting to connect via cookie... please wait...');
		if(typeof token != 'undefined' && typeof user != 'undefined') {
			socket.emit('auth', {user: user, token: token});
		}
		else {
			socket.emit('auth', {user: '', token: ''});
		}
	};
	socket.on('reconnect', doConnect);
	socket.on('connect', doConnect);
	socket.on('token', function(response) {
		writeCookie('loginuser', response.user, 30);
		writeCookie('logintoken', response.token, 30);
	});
	socket.on('edit', function(response) {
		response = _.extend({styleClass: 'terminal'}, response);
		doScroll = $('#bottom').isOnScreen();
		var last = $('#terminal > div:last-child');
		$form = $('<form class="editor ' + response.styleClass + '" data-command="' + response.command + '"><div class="edit_command">' + response.command + '</div><textarea>' + response.output + '</textarea><input type="submit" class="cancel button" value="Cancel"><input type="submit" class="submit button" value="Submit"></form>');
		$('#terminal').append($form);
		$form.find('textarea').focus();
		if(doScroll) {
			$('html, body').animate({scrollTop: $('#bottom').offset().top -20});
		}
		else {
			cmd.addClass('newmsg');
		}

	});

	$('#terminal').on('submit', '.editor', function(){
		var $form = $(this);
		var commandText = $form.data('command');

		commandText = commandText.replace(/\$edit/, $form.find('textarea').val());

		var command = {
			command: commandText,
			state: cmd.data('state')
		};
		socket.emit('command', command);
		$form.replaceWith('<div class="edit_complete">Editing submitted.</div>');
		cmd.focus();
		return false;
	});
	$('#terminal').on('click', '.editor .cancel', function(){
		var $form = $(this).closest('.editor');
		$form.replaceWith('<div class="edit_complete">Editing canceled.</div>');
		cmd.focus();
		return false;
	});


	$.fn.isOnScreen = function(){

		var win = $(window);

		var viewport = {
			top : win.scrollTop(),
			left : win.scrollLeft()
		};
		viewport.right = viewport.left + win.width();
		viewport.bottom = viewport.top + win.height();

		var bounds = this.offset();
		bounds.right = bounds.left + this.outerWidth();
		bounds.bottom = bounds.top + this.outerHeight();

		return (!(viewport.right < bounds.left || viewport.left > bounds.right || viewport.bottom < bounds.top || viewport.top > bounds.bottom));

	};

	cmd
		.on('keyup', function(ev){
			var cmd = $(this);
			switch(ev.keyCode) {
				// @todo implement arrow keys for history
				case 13:
					var command = {
						command: cmd.val(),
						state: cmd.data('state')
					};
					if(cmd.data('state') == 'command' && history[history.length - 1] != cmd.val()) {
						history.push(cmd.val());
					}
					socket.emit('command', command);
					historyIndex = 0;
					$(this).val('');
					return false;
					break;
				case 38: // Up
					if(historyIndex == 0) {
						historyCurrent = cmd.val();
					}
					historyIndex++;
					if(historyIndex > history.length) {
						historyIndex = history.length;
					}
					cmd.val(history[history.length - historyIndex]);
					break;
				case 40: // Down
					historyIndex--;
					if(historyIndex <= 0) {
						historyIndex = 0;
						cmd.val(historyCurrent);
					}
					else {
						cmd.val(history[history.length - historyIndex]);
					}
					break;
			}
		});
	$(window)
		.on('scroll', function() {
			doScroll = $('#bottom').isOnScreen();
			if(doScroll) {
				cmd.removeClass('newmsg');
			}
		})
		.on('mouseup', function(){
			if(getSelectionText() == '' && document.activeElement.tagName != 'TEXTAREA' && document.activeElement.tagName != 'INPUT') {
				cmd.focus();
			}
		});
	$('#terminal')
		.on('click', 'a.objectid', function(){
			cmd.val(cmd.val() + $(this).attr('href'));
			return false;
		})
		.on('click', 'a.execcommand', function(){
			var command = {
				command: $(this).attr('href'),
				state: cmd.data('state')
			};
			if(cmd.data('state') == 'command' && history[history.length - 1] != cmd.val()) {
				history.push($(this).attr('href'));
			}
			socket.emit('command', command);
			return false;
		});

});

