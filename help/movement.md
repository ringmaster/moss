#movement

Movement is achieved by putting the user's player thing into a different container.  Any thing can be a container for any other thing, but generally things through which players move are designated as "rooms".

Each room may contain multiple exits.  An exit can be used to move the current player via:

1. Invoke `go {direction}` where {direction} is one of the available exits.
2. Invoke `{direction}` where {direction} is one of the available exits.
3. Invoke an alias for the commonly used directions.

If an exit shares the name with another action, it cannot be invoked using method 2.  Instead, use method 1.

##Commonly used directions

* `n` - north
* `e` - east
* `w` - west
* `s` - south
* `ne` - northeast
* `nw` - northwest
* `se` - southeast
* `sw` - southwest
* `u` - up
* `d` - down
