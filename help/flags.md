#flags
Flags are attributes that are assigned to things that provide or remove certain capabilities to that thing or to players that attempt to act on that thing.

Flags that are boolean are enabled when they are set to a positive integer value, and disabled when they are set to 0.

* `hidden` - When enabled on a thing, the thing's description is not shown when a player uses `look`.  Useful for scenery.
* `fixed` - When enabled on a thing, the thing is fixed in place and cannot be taken by a player.

##Example usage

This will prevent a thing named "sofa" from appearing in the room contents listing when the player uses `look`:

`@attr sofa.flags.hidden=1`

This will restore the sofa's visibility:

`@attr sofa.flags.hidden=0`
