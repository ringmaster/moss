#commands
* `@me` - Shortcut to "@examine $me"
* `@help [topic]` - Display the help for a topic
* `look` - Show the current room description and contents.
* `look at the {thing}` - Shows the description for the thing.
* `@examine {thing}` - Shows the internal representation of a thing.
* `@create {thing}[={ancestor}]` - Creates a new thing, with an optional ancestor.
* `@describe {thing}[.{attribute}][={description}]` - Sets the description of a thing's basic or specified attribute.
* `@rename {thing}={newname}` - Renames a thing to a newname.
* `@clear {thing}.{attribute}` - Clears the value set for a thing's attribute.
* `@teleport [{thing}=]{destination}` - Moves the player or a thing to a named destination.
* `@dig {exit}[,{return}]={destination}` - Creates an named exit to a named destination, with optional return direction.
* `@undig {exit}` - Removes an existing exit.
* `go {direction}` - Travel through an existing exit.
* `say {speech}` - Speak speech aloud into the current room.
* `emote {emote}` - Emit an emote into the current room.
* `@adduser {username}[={ancestor}]` - Add the user username as a new user, with optional ancestor.
* `@setpass [{username}=]{password}` - Set the password for the current user or the specified user.
* `inventory` - List the contents of the player.
* `take {thing}` - Add a thing in the current room to the player contents.
* `drop {thing}` - Remove a thing from the player contents to the current room.
