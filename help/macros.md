#macros
Macros are strings that can be used in commands that are substituted with some other value before the command is processed.

* `$here` - Replaced with the object id of the current room.
* `$me` - Replaced with the object id of the current player.
* `$edit` - Replaced with the contents of the visual editor when it is submitted.

##Example usage

This will take a thing called "ball" and put it in the current player thing, the equivalent of the command `take ball`:
`@tel ball=$me`

This will put the thing called "ball" pack into the current room, regardless of its current location:
`@tel ball=$here`
