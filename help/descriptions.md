#descriptions

There are many descriptions available for a thing, each with its own use.  Descriptions are edited using the `@desc` command.

This is a list of the description types and their use:

* `basic` - This is the generic description of a thing.  If another description type is missing, this is used by default.
* `look` - Used when listing a rooms contents.
* `lookat` - Used when a player uses `look at {thing}` on a thing.
* `inventory` - Used when a player's inventory is displayed.

##Describing things

To set the description of a thing, use the thing's name and description type in the command:

`@desc sofa.lookat=It's long enough to lie down on, and soft.`

You can use a visual editor to edit the existing description of a thing by omitting the description itself from the command:

`@desc sofa.lookat`
