#creating

To create a new thing, use:

`@create {thing name}[={ancestor}]`

{thing name} can be any text that is valid for the name of a thing.

{ancestor} can be omitted, but it is useful for rooms to use "Room" as the ancestor.

When a new thing is created, it is placed in the current room.  This is a bit odd for new rooms, since they will appear in the current room's content list.  New rooms should have an additional command applied:

`@tel {new room name}=World`

This command moves the new room into the "World" container, where all rooms live, preventing the room from being listed in the current room's contents.
