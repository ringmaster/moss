function highlightJSON(json) {
	if (typeof json != 'string') {
		json = JSON.stringify(json, undefined, 2);
	}
	json = json.replace(/&/g, '&amp;').replace(/</g, '&lt;').replace(/>/g, '&gt;');
	json = json.replace(/("(\\u[a-zA-Z0-9]{4}|\\[^u]|[^\\"])*"(\s*:)?|\b(true|false|null)\b|-?\d+(?:\.\d*)?(?:[eE][+\-]?\d+)?)/g, function (match) {
		var cls = 'number';
		if (/^"/.test(match)) {
			if (/:$/.test(match)) {
				cls = 'key';
			} else {
				cls = 'string';
			}
		} else if (/true|false/.test(match)) {
			cls = 'boolean';
		} else if (/null/.test(match)) {
			cls = 'null';
		}
		return '<span class="' + cls + '">' + match + '</span>';
	});
	json = json.replace(/"([0-9a-f]{24})"/g, function(match, submatch) {
		return '<a class="objectid" href="#' + submatch + '">' + match + '</a>';
	});
	return json;
}

function sameThing(thing1, thing2) {
	return thing1._id.toString() == thing2._id.toString();
}

module.exports.highlightJSON = highlightJSON;
module.exports.sameThing = sameThing;