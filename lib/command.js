var Thing = require('./models/thing'),
	User = require('./models/user'),
	async = require('async'),
	fs = require('fs'),
	path = require('path'),
	mustache = require('mustache'),
	Utils = require('./utils'),
	_ = require('underscore'),
	marked = require('marked'),
	app = require('../app'),
	io = app.io;

// @todo This should probably not be HERE, but elsewhere
var myRenderer = new marked.Renderer();
myRenderer.link = function(href, title, text) {
	var a = '<a href="' + href +'" ';
	var classes = [];
	if(href[0] == '@') {
		classes.push('execcommand');
	}
	if(href[0] == '#') {
		classes.push('objectid');
	}
	a += 'class="' + classes.join(' ') + '" title="' + title + '">' + text + '</a>';
	return a;
};
marked.setOptions({
	renderer: myRenderer,
	gfm: true,
	tables: true,
	breaks: true,
	pedantic: false,
	sanitize: true,
	smartLists: true,
	smartypants: false
});

var Command = function(){
	this.commands = [];
};

Command.prototype.execute = function(socket, request, callback) {
	var i = 0,
		z = 0,
		params = {},
		map = [],
		result = false,
		self = this;

	if(typeof request == 'string') {
		request = {
			command: request
		};
	}

	// Too common aliases
	request.command = request.command.replace(/^n$/i, 'north');
	request.command = request.command.replace(/^e$/i, 'east');
	request.command = request.command.replace(/^w$/i, 'west');
	request.command = request.command.replace(/^s$/i, 'south');
	request.command = request.command.replace(/^ne$/i, 'northeast');
	request.command = request.command.replace(/^nw$/i, 'northwest');
	request.command = request.command.replace(/^se$/i, 'southeast');
	request.command = request.command.replace(/^sw$/i, 'southwest');
	request.command = request.command.replace(/^u$/i, 'up');
	request.command = request.command.replace(/^d$/i, 'down');

	// Replace macros
	request.command = request.command.replace(/\$here\b/ig, '#' + socket.player.thing.container);
	request.command = request.command.replace(/\$me\b/ig, '#' + socket.player.thing._id);

	var commands = self.defaultCommands();

	// @todo handle things that are commands in context

	async.series(
		[
			//exits
			function(callback){
				Thing.findById(socket.player.thing.container, function(err, room){
					if(room) {
						_.each(_.keys(room.exits), function (exit) {
							var re = new RegExp('^(' + exit + ')$', 'i');
							commands.push({match: re, fn: command.go, map: ['direction']});
						});
					}
					room.getContents(function(err, contents){
						_.each(contents, function (item) {
							if(typeof item.verbs != 'undefined') {
								_.each(item.verbs, function (verb) {
									commands.push({match: verb.match, fn: function (socket, request, callback) {
										request.params.verb = verb;
										command.thingVerbFn(socket, request, callback);
									}, map: verb.map});
								});
							}
						});
						callback(null, null);
					});
				});
			}
		],

		function(err, results) {
			// Match command handlers
			for(i; i < commands.length; i++) {
				if(result = request.command.match(commands[i].match)) {
					// If a submatch map was provided, map the submatches to named parameters
					if (typeof commands[i].map !== 'undefined' && commands[i].map.constructor == Array) {
						map = commands[i].map.slice(0);
						map.unshift("entire");
						for (z; z < map.length; z++) {
							params[map[z]] = result[z] ? result[z] : '';
						}
					}
					request.params = params;
					return commands[i].fn(socket, request, callback);
				}
			}

			callback('That command was not recognized.', null);
		}
	);
};

Command.prototype.defaultCommands = function() {
	var commands = [];
	commands.push(command.addCommand(/^@me$/, command.me));
	commands.push(command.addCommand(/^@help(?:\s+(.+))?$/, command.help, ['topic']));
	commands.push(command.addCommand(/\$edit/, command.edit));
	commands.push(command.addCommand(/^l(?:ook)?$/, command.look));
	commands.push(command.addCommand(/^l(?:ook)? (?:at the |at a |at an |at )?(.+)$/, command.lookAt, ['target']));
	commands.push(command.addCommand(/^@ex(?:amine)? (.+)$/, command.examine, ['thing']));
	commands.push(command.addCommand(/^@create\s+([^=]+?)(?:\s*=\s*(.+))?$/, command.create, ['newname', 'inherits']));
	commands.push(command.addCommand(/^@destroy\s+([^=]+?)$/, command.destroy, ['thingname']));
	commands.push(command.addCommand(/^@desc(?:ribe)?\s+([^\.]+)(?:\.([^=]+?))?$/, command.describeEdit, ['thingname', 'desctype']));
	commands.push(command.addCommand(/^@desc(?:ribe)?\s+([^\.]+)(?:\.(.+?))?\s*=\s*([\s\S]+)$/, command.describe, ['thingname', 'desctype', 'description']));
	commands.push(command.addCommand(/^@attr ([^\.]+)(?:\.(.+?))?\s*=\s*([\s\S]+)$/, command.attr, ['thingname', 'attribute', 'setting']));
	commands.push(command.addCommand(/^@rename ([^\.]+)\s*=\s*(.+)$/, command.rename, ['thingname', 'newname']));
	commands.push(command.addCommand(/^@clear ([^\.]+)(?:\.(.+?))?$/, command.clear, ['thingname', 'attribute', 'setting']));
	commands.push(command.addCommand(/^@tel(?:eport)? (?:([^=]+)\s*=)?\s*(.+)$/, command.teleport, ['target', 'destination']));
	commands.push(command.addCommand(/^@dig (\w+)\s*(?:,\s*(\w+))?\s*=\s*(.+)$/m, command.dig, ['direction', 'return', 'target']));
	commands.push(command.addCommand(/^@undig (\S+)$/m, command.unDig, ['direction']));
	commands.push(command.addCommand(/^go (.+)$/m, command.go, ['direction']));
	commands.push(command.addCommand(/^(?:"|say )(.+)$/m, command.say, ['speech']));
	commands.push(command.addCommand(/^(?::|emote )(.+)$/m, command.emote, ['speech']));
	commands.push(command.addCommand(/^@adduser\s+(.+)(?:\s+:([^:]))?$/, command.addUser, ['username', 'inherits']));
	commands.push(command.addCommand(/^@setpass (?:(\S+)\s*=)?\s*(.+)$/, command.setPass, ['username', 'password']));
	commands.push(command.addCommand(/^@listen (\S+)\.(\S+)?\s*=\s*(.+)$/, command.attr, ['thingname', 'event', 'data']));
	commands.push(command.addCommand(/^i(?:nventory)?$/, command.inventory));
	commands.push(command.addCommand(/^(?:take|get|pick up) (?:the |a |an )?(.+)$/, command.take, ['thing']));
	commands.push(command.addCommand(/^(?:drop|put down||leave) (?:the |a |an )?(.+)$/, command.drop, ['thing']));
	return commands;
};

Command.prototype.addCommand = function(match, implementation, argumentMap) {
	return {match: match, fn: implementation, map: argumentMap};
};

/**
 * Say something aloud in container of the commanding player
 */
Command.prototype.say = function(socket, request, callback) {
	var speech = request.params.speech;
	speech = speech.replace(/[<>'"&]/g, function(v){return '&#' + v.charCodeAt(0) + ';';}); // Fast dirty htmlEncode

	socket.emit('terminal', {output: 'You say, "' + speech + '"'});
	socket.player.thing.doSay(socket.player.thing.name + ' says, "' + speech + '"', function(){});
};

Command.prototype.emote = function(socket, request, callback) {
	var speech = request.params.speech;
	speech = speech.replace(/[<>'"&]/g, function(v){return '&#' + v.charCodeAt(0) + ';';}); // Fast dirty htmlEncode

	var output = socket.player.thing.name + ' ' + speech;
	socket.emit('terminal', {output: output});
	socket.player.thing.doEmote(output, function(){});
};

Command.prototype.me = function(socket, request, callback) {
	socket.emit('terminal', {output: '<pre>' + Utils.highlightJSON(JSON.stringify(socket.player.thing, undefined, 2)) + '</pre>', styleClass: 'system'});
	callback();
};

Command.prototype.look = function(socket, request, callback) {
	Thing.findById(socket.player.thing.container, function(err, room) {
		if(room) {
			var filename = path.join(process.cwd(), 'templates/look.mustache');
			fs.readFile(filename, function (err, template) {
				room.getContents(function(err, contents){
					contents = _.filter(contents, function(item) {
						ok = item._id.toString() != socket.player.thing._id.toString();
						// @todo filter out players that are not actively logged in
						ok &= !item.hasFlag('hidden');
						return ok;
					});
					async.map(
						contents,
						function(item, callback){
							item.getDescription('look', callback);
						},
						function(err, contents){
							var view = {
								room: room,
								title: room.name,
								description: marked(room.getDescription('basic')),
								contents: contents,
								exits: _.keys(room.exits)
							};
							var html = mustache.to_html(template.toString(), view);
							socket.emit('terminal', {output: html});
							callback();
						}
					);
				});
			});
		}
		else {
			callback('Room not found!', null);
		}
	});
};

Command.prototype.lookAt = function(socket, request, callback) {
	Thing.findOneLocal(request.params.target, socket.player, function(err, thing) {
		if(err) {
			socket.emit('terminal', {output: err, styleClass: 'error'});
		}
		else if(thing) {
			thing.getDescription('lookat', function(err, result){
				if(err) {
					socket.emit('terminal', {output: err, styleClass: 'error'});
				}
				else {
					socket.emit('terminal', {output: marked(result)});
				}
			});
		}
		else {
			socket.emit('terminal', {output: 'The requested thing was not found.', styleClass: 'error'});
		}
		callback();
	});
};

Command.prototype.examine = function(socket, request, callback) {

	Thing.findOneLocal(request.params.thing, socket.player, function(err, thing) {
		if(thing) {
			socket.emit('terminal', {output: 'Examining "' + thing.name + '" (<a class="objectid" href="#' + thing._id + '">' + thing._id + '</a>):', styleClass: 'system'});
			socket.emit('terminal', {output: '<pre>' + Utils.highlightJSON(JSON.stringify(thing, undefined, 2)) + '</pre>', styleClass: 'system'});
		}
		else {

			Thing.findOneAnywhere(request.params.thing, function(err, thing) {
				if (thing) {
					socket.emit('terminal', {output: 'Examining "' + thing.name + '" (<a class="objectid" href="#' + thing._id + '">' + thing._id + '</a>):', styleClass: 'system'});
					socket.emit('terminal', {output: '<pre>' + Utils.highlightJSON(JSON.stringify(thing, undefined, 2)) + '</pre>', styleClass: 'system'});
				}
				else {
					socket.emit('terminal', {output: 'The requested thing was not found.', styleClass: 'system'});
				}
			});
		}
		callback();
	});
};

Command.prototype.create = function(socket, request, callback) {
	var thingary = {
		name: request.params.newname
	};
	async.series([
			function(callback){
				if(request.params.inherits !== '') {
					Thing.findOne({name: request.params.inherits}, function(err, result) {
						if(result) {
							thingary.inherits = result._id;
							callback(null, true);
						}
						else {
							callback('Could not find the parent object, "' + request.params.inherits + '".', null);
						}
					});
				}
				else {
					callback(null, null);
				}
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				thingary.container = socket.player.thing.container;
				thing = new Thing(thingary);
				thing.save(function(err, savedThing){
					if(err) {
						socket.emit('terminal', {output: err, styleClass: 'systemerror'});
					}
					else {
						socket.emit('terminal', {output: 'Created new thing "' + savedThing.name + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'system'});
					}
				})
			}
		});
};

Command.prototype.describe = function(socket, request, callback) {
	var descType = 'description.basic';
	if(request.params.desctype != '') {
		descType = 'description.' + request.params.desctype;
	}
	command.setAttr(request.params.thingname, descType, request.params.description, socket, function(err, savedThing) {
		if(!err) {
			socket.emit('terminal', {output: 'Updated the description of "' + savedThing.name + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'system'});
		}
		callback(err);
	});
};

Command.prototype.setAttr = function(thingName, attribute, setting, socket, callback) {
	async.series([
			function(callback){
				Thing.findOneLocal(thingName, socket.player, function(err, thing) {
					if(thing) {
						callback(err, thing);
					}
					else {
						callback('Could not find the object, "' + thingName + '".', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				thing = results.pop();
				if(subAttrMatch = attribute.match(/^([^\.]+)\.([^\.]+)$/)) {
					attribute = subAttrMatch[1];
					var attr = thing[attribute];
					if(typeof attr == 'undefined') {
						attr = {};
					}
					attr[subAttrMatch[2]] = setting;
					thing[attribute] = attr;
				}
				else {
					thing[attribute] = setting;
				}
				thing.markModified(attribute);
				thing.save(function(err, savedThing){
					callback(null, savedThing);
				})
			}
		});
};

Command.prototype.clearAttr = function(thingName, attribute, socket, callback) {
	async.series([
			function(callback){
				Thing.findOneLocal(thingName, socket.player, function(err, thing) {
					if(thing) {
						callback(err, thing);
					}
					else {
						callback('Could not find the object, "' + thingName + '".', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				thing = results.pop();
				if(subAttrMatch = attribute.match(/^([^\.]+)\.([^\.]+)$/)) {
					attribute = subAttrMatch[1];
					thing[attribute] = _.omit(thing[attribute], subAttrMatch[2]);
				}
				else {
					thing[attribute] = undefined;
				}
				thing.markModified(attribute);
				thing.save(function(err, savedThing){
					callback(null, savedThing);
				})
			}
		});
};

Command.prototype.dig = function(socket, request, callback) {
	async.series([
			function(callback){
				Thing.findById(socket.player.thing.container, function(err, thing) {
					if(thing) {
						callback(null, thing);
					}
					else {
						callback('Could not find the current room.', null);
					}
				});
			},
			function(callback){
				Thing.byName(request.params.target, function(err, thing) {
					if(thing) {
						callback(null, thing);
					}
					else {
						callback('Could not find the target room.', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				sourceRoom = results.shift();
				destRoom = results.shift();
				var newExits = {};
				newExits[request.params.direction] = destRoom._id;
				sourceRoom.exits = _.extend(sourceRoom.exits || {}, newExits);

				sourceRoom.markModified('exits');
				sourceRoom.save(function(err, savedThing){
					if(request.params.return != '') {
						var newExits = {};
						newExits[request.params.return] = sourceRoom._id;
						destRoom.exits = _.extend(destRoom.exits || {}, newExits);
						destRoom.markModified('exits');
						destRoom.save(function(err, savedThing) {
							socket.emit('terminal', {output: 'Created exit "' + request.params.direction + '" to ' + destRoom.name + ' with return exit "' + request.params.return + '".', styleClass: 'system'});
							callback(null, savedThing);
						});
					}
					else {
						socket.emit('terminal', {output: 'Created exit "' + request.params.direction + '" to ' + destRoom.name + '.', styleClass: 'system'});
						callback(null, savedThing);
					}
				})
			}
		});
};

Command.prototype.unDig = function(socket, request, callback) {
	Thing.findById(socket.player.thing.container, function(err, room) {
		if(!room) {
			callback('Could not find the current room.', null);
		}
		else {
			room.exits = _.omit(room.exits, request.params.direction);
			room.markModified('exits');
			room.save(function(err, savedRoom){
				socket.emit('terminal', {output: 'Removed exit "' + request.params.direction + '" to ' + room.name + '.', styleClass: 'system'});
			});
			Thing.findById(room.exits[request.params.direction], function(err, destRoom) {
				if(destRoom) {
					if(exitName = _.invert(destRoom.exits)[room._id]) {
						destRoom.exits = _.omit(destRoom.exits, exitName);
						destRoom.markModified('exits');
						destRoom.save(function(err, savedDestRoom) {
							socket.emit('terminal', {output: 'Removed return exit "' + exitName + '" to ' + room.name + '.', styleClass: 'system'});
						});
					}
				}
			});
		}
	});


	async.series([
			function(callback){
			},
			function(callback){
				Thing.byName(request.params.direction, function(err, thing) {
					if(thing) {
						callback(null, thing);
					}
					else {
						callback('Could not find the target room.', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				sourceRoom = results.shift();
				destRoom = results.shift();
				var newExits = {};
				newExits[request.params.direction] = destRoom._id;
				sourceRoom.exits = _.extend(sourceRoom.exits || {}, newExits);

				sourceRoom.markModified('exits');
				sourceRoom.save(function(err, savedThing){
					if(request.params.return != '') {
						var newExits = {};
						newExits[request.params.return] = sourceRoom._id;
						destRoom.exits = _.extend(destRoom.exits || {}, newExits);
						destRoom.markModified('exits');
						destRoom.save(function(err, savedThing) {
							socket.emit('terminal', {output: 'Created exit "' + request.params.direction + '" to ' + destRoom.name + ' with return exit "' + request.params.return + '".', styleClass: 'system'});
							callback(null, savedThing);
						});
					}
					else {
						socket.emit('terminal', {output: 'Created exit "' + request.params.direction + '" to ' + destRoom.name + '.', styleClass: 'system'});
						callback(null, savedThing);
					}
				})
			}
		});
};

Command.prototype.attr = function(socket, request, callback) {
	command.setAttr(request.params.thingname, request.params.attribute, request.params.setting, socket, function(err, savedThing) {
		if(!err) {
			socket.emit('terminal', {output: 'Updated ' + request.params.attribute + ' for "' + savedThing.name + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'system'});
		}
		callback(err);
	});
};

Command.prototype.clear = function(socket, request, callback) {
	command.clearAttr(request.params.thingname, request.params.attribute, socket, function(err, savedThing) {
		if(!err) {
			socket.emit('terminal', {output: 'Cleared ' + request.params.attribute + ' for "' + savedThing.name + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'system'});
		}
		callback(err);
	});
};

Command.prototype.teleport = function(socket, request, callback) {
	async.series([
			function(callback){
				if(request.params.target == "") {
					return callback(null, socket.player.thing);
				}
				else {
					Thing.findOneLocal(request.params.target, socket.player, function (err, thing) {
						if(err) {
							callback(err, thing);
						}
						else if (thing) {
							callback(null, thing);
						}
						else {
							callback('Could not find the thing, "' + request.params.target + '".', null);
						}
					});
				}
			},
			function(callback){
				Thing.findOneAnywhere(request.params.destination, function (err, thing) {
					if (thing) {
						callback(null, thing);
					}
					else {
						callback('Could not find the destination, "' + request.params.destination + '".', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				thing = results.shift();
				destination = results.shift();
				thing.container = destination._id;
				thing.markModified('container');
				thing.save(function(err, savedThing){
					socket.emit('terminal', {output: 'Teleported ' + thing.name + ' to ' + destination.name + '.', styleClass: 'system'});
				})
			}
		});
};

Command.prototype.go = function(socket, request, callback) {
	async.series([
			function(callback){
				Thing.findOne({_id: socket.player.thing.container}, function(err, room) {
					if(room) {
						callback(err, room);
					}
					else {
						callback('Could not find the current room!', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'error'});
			}
			else {
				room = results.shift();

				if(target = room.exits[request.params.direction]) {
					Thing.findById(target, function(err, destRoom){
						if(destRoom) {
							socket.player.thing.container = destRoom._id;
							socket.player.thing.markModified('container');
							socket.player.thing.save(function (err, playerThing) {
								command.look(socket, request, function(err, lookResult) {
									callback(err, playerThing);
								});
							});
						}
						else {
							socket.emit('terminal', {output: 'The destination room no longer exists.', styleClass: 'systemerror'});
						}
					});
				}
				else {
					socket.emit('terminal', {output: 'That exit is not defined.', styleClass: 'error'});
				}
			}
		});
};

Command.prototype.addUser = function(socket, request, callback) {
	var thingary = {
		name: request.params.username
	};
	async.series([
			function(callback){
				if(request.params.inherits == '') {
					request.params.inherits = 'Player';
				}
				Thing.findOne({name: request.params.inherits}, function(err, parent) {
					if(parent) {
						callback(null, parent);
					}
					else {
						callback('Could not find the parent object, "' + request.params.inherits + '".', null);
					}
				});
			}
		],
		function(err, results){
			if(err) {
				socket.emit('terminal', {output: err, styleClass: 'systemerror'});
			}
			else {
				thingary.container = socket.player.thing.container;
				thing = new Thing({
					name: request.params.username,
					inherits: results.shift(),
					container: socket.player.thing.container,
					flags: {
						player: 1
					}
				});
				console.log(thing);
				thing.save(function(err, savedThing){
					if(savedThing) {
						var user = new User({username: request.params.username, password: 'newuserpassword', mossId: savedThing._id});
						user.save(function (err, saveUser) {
							socket.emit('terminal', {output: 'Created new user "' + savedThing.name + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'system'});
						});
					}
					else {
						socket.emit('terminal', {output: JSON.stringify(err), styleClass: 'systemerror'});
					}
				})
			}
		});
};

Command.prototype.setPass = function(socket, request, callback) {
	if(request.params.username == '') {
		request.params.username = socket.player.user.username;
	}
	User.findOne({username: request.params.username}, function(err, user) {
		if(user) {
			user.password = request.params.password;
			user.save(function(err, user) {
				socket.emit('terminal', {output: 'Set password for user "' + user.username + '".', styleClass: 'system'});
			});
		}
		else {
			socket.emit('terminal', {output: 'Unable to find the user "' + request.params.username + '".', styleClass: 'systemerror'});
		}
	});
};

Command.prototype.listen = function(socket, request, callback) {
	Thing.findOneLocal(request.params.thingname, socket.player, function(err, thing) {
		if(thing) {
			thing.doListen(request.params.event, socket.player.thing, request.params.data);
		}
		else {
			socket.emit('terminal', {output: 'Unable to find the thing "' + request.params.thingname + '".', styleClass: 'systemerror'});
		}
	});
};


Command.prototype.inventory = function(socket, request, callback) {
	var filename = path.join(process.cwd(), 'templates/inventory.mustache');
	fs.readFile(filename, function (err, template) {
		socket.player.thing.getContents(function(err, contents){
			contents = _.map(contents, function(item) {
				return marked(item.getDescription('inventory'));
			});
			var view = {
				player: socket.player.thing,
				contents: contents
			};
			var html = mustache.to_html(template.toString(), view);
			socket.emit('terminal', {output: html});
			callback();
		});
	});
};

Command.prototype.take = function(socket, request, callback) {

	Thing.findOneLocal(request.params.thing, socket.player, function(err, thing) {
		if(thing) {
			if(thing.hasFlag('fixed')) {
				socket.emit('terminal', {output: "That's fixed in place!", styleClass: 'error'});
			}
			else if(Utils.sameThing(thing, socket.player.thing)) {
				socket.emit('terminal', {output: "You can't take yourself!", styleClass: 'error'});
			}
			else if(thing.hasFlag('player')) {
				socket.emit('terminal', {output: "You can't take other players!", styleClass: 'error'});
			}
			else {
				thing.doEmit(socket.player.thing.name + ' takes the ' + thing.name + '.', function(){});
				thing.container = socket.player.thing._id;
				thing.markModified('container');
				thing.save(function(err, savedThing){
					socket.emit('terminal', {output: 'You take the ' + savedThing.name + '.'});
				});
			}
		}
		else {
			socket.emit('terminal', {output: 'The requested thing is not here.', styleClass: 'error'});
		}
		callback();
	});
};

Command.prototype.drop = function(socket, request, callback) {

	Thing.findOneLocal(request.params.thing, socket.player, function(err, thing) {
		if(thing) {
			if(thing.container.toString() == socket.player.thing._id.toString()) {
				thing.container = socket.player.thing.container;
				thing.markModified('container');
				thing.save(function (err, savedThing) {
					socket.emit('terminal', {output: 'You drop the ' + savedThing.name + '.'});
					thing.doEmit(socket.player.thing.name + ' drops the ' + savedThing.name + '.', function () {
					});
				});
			}
			else {
				socket.emit('terminal', {output: 'You are not carrying that.', styleClass: 'error'});
			}
		}
		else {
			socket.emit('terminal', {output: 'The requested thing is not here.', styleClass: 'error'});
		}
		callback();
	});
};

Command.prototype.rename = function(socket, request, callback) {
	command.setAttr(request.params.thingname, 'name', request.params.newname, socket, function(err, savedThing) {
		if(!err && savedThing) {
			socket.emit('terminal', {output: 'Updated the name of "' + request.params.thingname + '" to "' + savedThing.name + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'system'});
		}
		else {
			socket.emit('terminal', {output: 'Could not rename "' + request.params.thingname + '" to "' + request.params.newname + '" (<a class="objectid" href="#' + savedThing._id + '">' + savedThing._id + '</a>).', styleClass: 'systemerror'});
		}
		callback(err);
	});
};

Command.prototype.edit = function(socket, request, callback) {
	socket.emit('edit', {command: request.command, output: ''});
};

Command.prototype.describeEdit = function(socket, request, callback) {
	var desctype = (request.params.desctype == '') ? 'basic' : request.params.desctype;
	Thing.findOneLocal(request.params.thingname, socket.player, function(err, thing){
		if(typeof thing.description == 'undefined' || thing.description[desctype] == 'undefined') {
			output = '';
		}
		else {
			output = thing.description[desctype];
		}
		socket.emit('edit', {command: '@desc ' + request.params.thingname + '.' + desctype + '=$edit', output: output});
	});
};

Command.prototype.thingVerbFn = function(socket, request, callback) {
	socket.emit('terminal', {output: highlightJSON(JSON.stringify(request.params))});
};

Command.prototype.help = function(socket, request, callback) {
	var helpfile = request.params.topic.replace(/[^a-z0-9]/gi, '_');
	if(helpfile == '') {
		helpfile = 'help';
	}
	filename = path.join(process.cwd(), 'help', helpfile + '.md');
	fs.exists(filename, function(exists){
		var extension, mimeType, fileStream;
		if (exists) {
			fs.readFile(filename, function (err, template) {
				if (err) {
					socket.emit('terminal', {output: 'There was a problem reading that help file.  Try using <a class="execcommand" href="@help">@help</a> without a topic.', styleClass: 'systemerror'});
				}
				else {
					var html = marked(template.toString());
					socket.emit('terminal', {output: html, styleClass: 'help'});
				}
			});

		} else {
			socket.emit('terminal', {output: 'That help topic doesn\'t exist.  Try using <a class="execcommand" href="@help">@help</a> without a topic.', styleClass: 'systemerror'});
		}
	});
};

Command.prototype.destroy = function(socket, request, callback) {
	Thing.findOneLocal(request.params.thingname, socket.player, function(err, thing){
		if(err) {
			socket.emit('terminal', {output: err, styleClass: 'systemerror'});
		}
		else if(thing) {
			if(Utils.sameThing(thing, socket.player.thing)) {
				socket.emit('terminal', {output: "You can't destroy yourself!", styleClass: 'systemerror'});
			}
			else {
				thing.remove(function (err, removedThing) {
					if (err) {
						socket.emit('terminal', {output: err, styleClass: 'systemerror'});
					}
					else {
						socket.emit('terminal', {output: 'Destroyed "' + removedThing.name + '" (<a class="objectid" href="#' + removedThing._id + '">' + removedThing._id + '</a>).', styleClass: 'system'});
					}
				})
			}
		}
		else{
			socket.emit('terminal', {output: 'Could not destroy "' + request.params.thingname + '".', styleClass: 'systemerror'});
		}
	});
};


command = new Command();

module.exports = command;
