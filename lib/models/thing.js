var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	_ = require('underscore'),
	app = require('../../app'),
	io = app.io;

var ThingSchema = Schema({
	name: { type: String, required: true, index: { unique: false } },
	title: String,
	description: Schema.Types.Mixed,
	verbs: Schema.Types.Mixed,
	type: String,
	flags: Schema.Types.Mixed,
	inherits: Schema.ObjectId,
	container: Schema.ObjectId,
	exits: Schema.Types.Mixed
}, { strict: false });

ThingSchema.methods.getDescription = function(descType, callback) {
	var description, err = null;

	if(this.flagExists(descType)) {
		descType = this.flags[descType];
	}

	if(typeof this.description != 'undefined') {

		if(typeof this.description[descType] != 'undefined') {
			description = this.description[descType];
		}
		else if(typeof this.description['basic'] != 'undefined') {
			description = this.description.basic;
		}
		else {
			description = 'The basic description for the ' + this.name + ' has not been defined!';
			err = 'The basic description for the ' + this.name + ' has not been defined!';
		}
	}
	else {
		description = 'The ' + this.name + ' has not been described!';
		err = 'The ' + this.name + ' has not been described!';
	}
	if(typeof callback != 'undefined') {
		callback(err, description);
	}
	else {
		return description;
	}
};

ThingSchema.methods.getContents = function(callback) {
	var self = this;
	Thing.find({container: self._id}, function(err, contents) {
		callback(err, contents);
	});
};

ThingSchema.methods.getContainer = function(callback) {
	var self = this;
	Thing.findById(self.container, function(err, container) {
		callback(err, container);
	});
};

/**
 * Returns true if the flag exists and is a positive integer
 * @param flag
 * @returns {*|boolean}
 */
ThingSchema.methods.hasFlag = function(flag) {
	return this.flagExists(flag) && parseInt(this.flags[flag]) > 0;
};

/**
 * Returns true if the flag key exists, regardless of value
 * @param flag
 * @returns {*}
 */
ThingSchema.methods.flagExists = function(flag) {
	if(typeof this.flags == 'undefined' || this.flags == null) return false;
	return _.has(this.flags, flag);
};

/**
 * Call the bound methods on all of:
 *  . This thing's container.
 *  . The things in this thing's container.
  */

ThingSchema.methods.trigger = function(event, callback) {
	var self = this;
	Thing.findById(self.container, function(err, container) {
		Thing.find({container: container._id}, function(err, contents) {
			_.each(contents, function(item) {
				item.emit();
			});
			callback(err, null);
		});
	});
};

ThingSchema.methods.doListen = function(event, emitter, data) {
	var self = this;
	var playerClient = null;

	// Is this thing an active player?
	var isPlayer = _.reduce(app.io.sockets.clients(), function(memo, client){
		var p = (
			(typeof client.player.thing !== "undefined")
			&& (client.player.thing._id.toString() == self._id.toString())
			&& (client.player.thing._id.toString() != emitter._id.toString())
		);

		if(p) {
			playerClient = client;
		}
		return memo || p;
	}, false);

	if(isPlayer) {
		switch (event) {
			case 'say':
				playerClient.emit('terminal', {output: data, styleClass: 'say'});
				break;
			case 'emote':
				playerClient.emit('terminal', {output: data, styleClass: 'emote'});
				break;
			case 'emit':
				playerClient.emit('terminal', {output: data, styleClass: 'emit'});
				break;
		}
	}
	// @todo look for an event in an event list and execute it.

};

ThingSchema.methods.doEvent = function(event, data, range, callback) {
	var self = this;

	self.getContainer(function(err, container){
		container.getContents(function(err, contents) {
			_.each(contents, function(thing){
				thing.doListen(event, self, data);
			});
		});
	});

	callback(null, null);
};

ThingSchema.methods.doSay = function(text, callback) {
	this.doEvent('say', text, 0, callback);
};

ThingSchema.methods.doEmote = function(text, callback) {
	this.doEvent('emote', text, 0, callback);
};

ThingSchema.methods.doEmit = function(text, callback) {
	this.doEvent('emit', text, 0, callback);
};

ThingSchema.statics.byName = function(name, callback) {
	var re = new RegExp('\\b' + name + '\\b', 'i');
	Thing.findOne({name: re}, function(err, contents) {
		callback(err, contents);
	});
};

ThingSchema.statics.findOneLocal = function(name, player, callback) {
	var query = Thing.find();
	if(name[0] == '#') {
		query.$where('this._id.toString().match(/' + name.substr(1) + '/i);');
	}
	else {
		re = new RegExp('\\b' + name + '\\b', 'i');
		query.where('name', re);
		query.or([
			{container: player.thing._id},
			{container: player.thing.container},
			{_id: player.thing.container}
		]);
	}

	query.exec(function(err, contents) {
		if(typeof contents == 'undefined') {
			err = 'Nothing was found matching that criteria.';
		}
		else if(contents.length > 1) {
			err = 'You need to be more specific.';
		}
		else {
			contents = contents.pop();
		}
		callback(err, contents);
	});
};

ThingSchema.statics.findOneAnywhere = function(name, callback) {
	var query = Thing.find();
	if(name[0] == '#') {
		query.$where('this._id.toString().match(/' + name.substr(1) + '/i);');
	}
	else {
		re = new RegExp('\\b' + name + '\\b', 'i');
		query.where('name', re);
	}

	query.exec(function(err, contents) {
		if(typeof contents == 'undefined') {
			err = 'Nothing was found matching that criteria.';
		}
		else if(contents.length > 1) {
			err = 'You need to be more specific.';
		}
		else {
			contents = contents.pop();
		}
		callback(err, contents);
	});
};

mongoose.set('debug', true);

var Thing = mongoose.model('Thing', ThingSchema);

module.exports = Thing;