var mongoose = require('mongoose'),
	Schema = mongoose.Schema,
	bcrypt = require('bcryptjs'),
	async = require('async'),
	SALT_WORK_FACTOR = 10;

var UserSchema = Schema({
		username: { type: String, required: true, index: { unique: true } },
		password: { type: String, required: true },
		mossId: Schema.ObjectId,
		token: { type: String }
	});

UserSchema.pre('save', function(next) {
	var user = this;
	
	async.parallel(
		[
			function(next){
				// only hash the password if it has been modified (or is new)
				if (!user.isModified('password')) return next();

				// generate a salt
				bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
					if (err) return next(err);

					// hash the password along with our new salt
					bcrypt.hash(user.password, salt, function(err, hash) {
						if (err) return next(err);

						// override the cleartext password with the hashed one
						user.password = hash;

						next();
					});
				});
			},
			function(next){
				// only hash the token if it has been modified (or is new)
				if (!user.isModified('token')) return next();

				// generate a salt
				bcrypt.genSalt(SALT_WORK_FACTOR, function(err, salt) {
					if (err) return next(err);

					// hash the token along with our new salt
					bcrypt.hash(user.token, salt, function(err, hash) {
						if (err) return next(err);

						// override the cleartext token with the hashed one
						user.token = hash;

						next();
					});
				});
			}
		],
		next
	);

});

UserSchema.methods.comparePassword = function(candidatePassword, callback) {
	bcrypt.compare(candidatePassword, this.password, function(err, isMatch) {
		if (err) return callback(err);
		callback(null, isMatch);
	});
};

UserSchema.methods.compareToken = function(candidatePassword, callback) {
	if(typeof this.token == 'undefined') {
		callback(null, false);
	}
	else {
		try {
			bcrypt.compare(candidatePassword, this.token, function (err, isMatch) {
				if (err) return callback(err);
				callback(null, isMatch);
			});
		}
		catch(e) {
			callback(null, false);
		}
	}
};

module.exports = mongoose.model('User', UserSchema);