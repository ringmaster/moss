var config = require('./config'),
	_ = require('underscore');

config.server = _.extend({
		name: 'Moss',
		version: '1.0.0',
		website: 'https://bitbucket.org/ringmaster/moss'
	}, config.server);

function debug(value) {
	if(typeof config.server.debug !== undefined && config.server.debug) {
		console.log(value);
	}
}

debug(config);

var http = require('http'),
	fs = require('fs'),
	url = require('url'),
	path = require('path'),
	mustache = require('mustache'),
	async = require('async'),
	mongoose = require('mongoose'),
	crypto = require('crypto'),
	User = require('./lib/models/user'),
	Thing = require('./lib/models/thing'),
	Command = reloadable('./lib/command', path.join(process.cwd(), './lib/command.js')), //require('./lib/command'),
	server = http.createServer(function (req, res) {
		var uri, filename;
		var mimeTypes = {
			'html': 'text/html', 'png': 'image/png',
			'js': 'text/javascript', 'css': 'text/css',
			'svg': 'image/svg+xml'
		};

		uri = url.parse(req.url).pathname;
		uri = uri.replace(/\.+/g, '.').replace(/\/+/g, '/').replace(/[^a-z0-9\-\.\/_]+/g, '');
		switch(uri) {
			case '/':
			case '':
				uri = '/index.html';
				break;
		}
		filename = path.join(process.cwd(), 'public', uri);
		fs.exists(filename, function(exists){
			var extension, mimeType, fileStream;
			if (exists) {
				extension = path.extname(filename).substr(1);
				mimeType = mimeTypes[extension] || 'application/octet-stream';
				res.writeHead(200, {'Content-Type': mimeType});
				console.log('serving ' + filename + ' as ' + mimeType);

				fs.readFile(filename, function (err, template) {
					if (err) {
						throw err;
					}

					var html = mustache.to_html(template.toString(), config);
					res.write(html);
					res.end();
				});

			} else {
				console.log('not exists: ' + filename);
				res.writeHead(404, {'Content-Type': 'text/plain'});
				res.write('404 Not Found\n');
				res.end();
			}
		});
	});

var io = require('socket.io').listen(server);
//io.set('log level', 1);
module.exports.io = io;

async.series([
	function(callback) {
		debug('Starting web server');
		server.listen(config.server.port);
		callback(null, true);
	},
	function(callback) {
		debug('Connecting to database');
		mongoose.connect(config.server.database);
		callback(null, true);
	}],
	function(err, results) {
		debug('Listening for socket connections');
		io.on('connection', function (socket) {
			socket.player = {
				state: 'login'
			};

			socket.on('command', function(request){
				var response = {};

				switch(socket.player.state) {
					case 'login':
						socket.player.state = 'password';
						socket.player.username = request.command;
						socket.emit('terminal', {output: "Password for " + request.command + ':', state: 'password', styleClass: 'comms'});
						break;
					case 'password':
						socket.player.state = 'wait';
						socket.emit('terminal', {output: "Received password for " + socket.player.username + '...  Logging in...', state: 'wait', styleClass: 'comms'});

						User.findOne({username: socket.player.username}, function(err, user){
							if(user == null) {
								socket.player.state = 'login';
								socket.emit('terminal', {output: "That user record was not found.  Please try again.", state: 'login', styleClass: 'comms'});
								socket.emit('terminal', {output: 'Username:', state: 'login'});
							}
							else {
								user.comparePassword(request.command, function(err, passwordCorrect){
									var token = crypto.createHash('sha1').update(new Date().toString()).digest('base64');
									user.token = token;
									user.markModified('token');
									user.save();
									socket.emit('token', {user: user.username, token: token});
									if(passwordCorrect) {
										Thing.findById(user.mossId, function(err, thing) {
											socket.emit('terminal', {output: "Logged in user " + socket.player.username + '.', state: 'command', styleClass: 'comms'});
											socket.player = _.extend(socket.player, {state: 'command', loggedIn: true, user: user, thing: thing});
											socket.emit('terminal', {output: '<p>Help is available via <a href="@help" class="execcommand">@help</a>.</p>', styleClass: 'help'});
											Command.execute(socket, {command: 'look'}, function(){});
										});
									}
									else {
										socket.player.state = 'login';
										socket.emit('terminal', {output: "That user record was not found.  Please try again.", state: 'login', styleClass: 'comms'});
										socket.emit('terminal', {output: 'Username:', state: 'login', styleClass: 'comms'});
									}
								});
							}

						});
						break;
					case 'command':
						Thing.findById(socket.player.user.mossId, function(err, thing) {
							socket.player = _.extend(socket.player, {thing: thing});

							socket.emit('terminal', {output: request.command, styleClass: 'command'});
							try {
								Command.execute(socket, request, function (err, result) {
									if (err) {
										socket.emit('terminal', {output: err, styleClass: 'error'});
									}
								});
							}
							catch(e) {
								console.log(request);
								console.log(e);
								var err = 'That command resulted in a system error.\n<br>' + e;
								socket.emit('terminal', {output: err, styleClass: 'systemerror'});
							}

						});
						break;
					default:
						response.output = "Sent: " + request.command;
						socket.emit('terminal', response);
						break;
				}
			});

			socket.on('auth', function(request){
				if(typeof request.user !== 'undefined' && typeof request.token !== 'undefined' && request.user != '' && request.token != '') {
					User.findOne({username: request.user}, function (err, user) {
						console.log('Looked up user:' + request.user);
						if(user) {
							user.compareToken(request.token, function (err, matches) {
								if (matches) {
									socket.player.state = 'command';
									socket.player.username = user.username;

									Thing.findById(user.mossId, function (err, thing) {
										socket.emit('terminal', {output: "Logged in user " + socket.player.username + ' via cookie.', state: 'command', styleClass: 'comms'});
										socket.player = _.extend(socket.player, {state: 'command', loggedIn: true, user: user, thing: thing});
										socket.emit('terminal', {output: '<p>Help is available via <a href="@help" class="execcommand">@help</a>.</p>', styleClass: 'help'});
										Command.execute(socket, {command: 'look'}, function () {
										});
									});
								}
								else {
									socket.emit('terminal', {output: "Login for user " + request.user + ' via cookie failed.', state: 'login', styleClass: 'comms'});
								}
							});
						}
						else {
							socket.emit('terminal', {output: 'Username:', state: 'login', styleClass: 'comms'});
						}
					});
				}
				else {
					socket.emit('terminal', {output: 'Username:', state: 'login', styleClass: 'comms'});
				}
			});
		});
	}
);

function reloadable(modulename, versionfile) {
	var mymodule = require(modulename);
	fs.watchFile(versionfile, function (current, previous) {
		if (current.mtime.toString() !== previous.mtime.toString()) {
			console.log('reloading module:' + modulename);
			delete require.cache[require.resolve(modulename)];
			mymodule = require(modulename);
		}
	});
	return mymodule;
}
