## Installation Instructions
1. Clone the repo.
2. Run `npm install` in the project directory to download the required packages to node_modules
3. Set the Mongo connection details in config.js
4. Run `mongorestore initialdb` to import the initial database.
5. Run `node app` to start the server.
6. Connect to the configured web server to begin a session.

